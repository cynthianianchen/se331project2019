import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge, BehaviorSubject } from 'rxjs';
import Student from 'src/app/entity/student';

const EXAMPLE_DATA: Student[] = [ ];

export class AdminDashboardDataSource extends DataSource<Student> {
  data: Student[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;
  filter$: BehaviorSubject<string>;
  constructor() {
    super();
  }

  connect(): Observable<Student[]> {

    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange,
      this.filter$.asObservable()
    ];

    this.paginator.length = this.data.length;
    return merge(...dataMutations).pipe(map(() => {
      return this.getFilter(this.getPagedData(this.getSortedData([...this.data])));
    }));
  }

  disconnect() {}

  private getPagedData(data: Student[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  private getSortedData(data: Student[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
  private getFilter(data: Student[]): Student[] {
    const filter = this.filter$.getValue();
    if (filter === '') {
      return data;
    }
    return data.filter((student) => {
      return (student.name.toLowerCase().includes(filter) || student.name.toUpperCase().includes(filter) ||
      student.studentId.toLowerCase().includes(filter));
    });
  }
}


function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
