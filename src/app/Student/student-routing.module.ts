import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsComponent } from './list/students.component';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from 'src/app/shared/file-not-found/file-not-found.component';
import { StudentsAddComponent } from './register/students.add.component';



const StudentRoutes: Routes = [
  { path: 'view', component: FileNotFoundComponent },
  { path: 'add', component: StudentsAddComponent },
  { path: 'list', component: StudentsComponent },
  { path: 'detail/:id', component: StudentsViewComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(StudentRoutes)
  ],
  exports: [
    RouterModule
  ] 
})
export class StudentRoutingModule { }
