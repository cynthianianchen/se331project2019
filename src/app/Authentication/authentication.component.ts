import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  constructor(private router: Router) { }

  username:string;
  password:string;

  ngOnInit() {
  }

  login(){
    if(this.username == 'admin' && this.password == 'admin'){
      
      this.router.navigate(["admin"]);
      const Toast = Swal.mixin({
       toast: true,
       position: 'top-end',
       showConfirmButton: false,
       timer: 3000
     })
      Toast.fire({
       type: 'success',
       title: 'Signed in successfully'
     })
     } 
     else if (this.username == 'teacher' && this.password == 'teacher') {
       this.router.navigate(["teacher"]);
       const Toast = Swal.mixin({
         toast: true,
         position: 'top-end',
         showConfirmButton: false,
         timer: 3000
       })
       Toast.fire({
         type: 'success',
         title: 'You have logged into the LTAS successfully'
       })
     } 
     else if (this.username == 'student' && this.password == 'student') {
       this.router.navigate(["student"]);
       const Toast = Swal.mixin({
         toast: true,
         position: 'top-end',
         showConfirmButton: false,
         timer: 3000
       })
       Toast.fire({
         type: 'success',
         title: 'You have logged into the LTAS successfully'
       })
     } else {
       const Toast = Swal.mixin({
         toast: true,
         position: 'top-end',
         showConfirmButton: false,
         timer: 3000
       })
       Toast.fire({
         type: 'error',
         title: 'You filed the login'
       })
      } 

  }

}
