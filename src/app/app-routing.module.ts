import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from 'src/app/shared/file-not-found/file-not-found.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { AdminDashboardComponent } from './Admin/admin-dashboard/admin-dashboard.component';
import { TeacherDashboardComponent } from './Teacher/teacher-dashboard/teacher-dashboard.component';
import { StudentDashboardComponent } from './Student/student-dashboard/student-dashboard.component';
import {MyNavComponent} from './my-nav/my-nav.component';

const appRoutes: Routes = [

  {path:'', redirectTo:'/main', pathMatch:'full'},

  { path : 'my-nav', component:MyNavComponent},

  { path: 'Authentication', component: AuthenticationComponent},
  { path: 'admin-dashboard', component: AdminDashboardComponent },
  { path: 'teacher-dashboard', component: TeacherDashboardComponent },
  { path: 'student-dashboard', component:  StudentDashboardComponent},
  { path: '**', component: FileNotFoundComponent }

];

@NgModule({
   imports:[
       RouterModule.forRoot(appRoutes)
   ],
   exports:[
       RouterModule
   ]
})
export class AppRoutingModule{

}
 
