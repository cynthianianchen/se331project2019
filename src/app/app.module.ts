import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatFormFieldModule , MatInputModule, MatCardModule} from '@angular/material';
import { AuthenticationComponent } from './authentication/authentication.component';
import { AdminRegisterComponent } from './Admin/admin-register/admin-register.component';
import { AdminDashboardComponent } from './Admin/admin-dashboard/admin-dashboard.component';
import { StudentsComponent } from './Student/list/students.component';
import { StudentsAddComponent } from './Student/register/students.add.component';
import { StudentsViewComponent } from './Student/view/students.view.component';
import { StudentTableComponent } from './Student/student-table/student-table.component';
import { StudentRoutingModule } from './Student/student-routing.module';
import { TeacherDashboardComponent } from './Teacher/teacher-dashboard/teacher-dashboard.component';
import { StudentDashboardComponent } from './Student/student-dashboard/student-dashboard.component';
import { ViewCourseComponent } from './Teacher/view-course/view-course.component';
import { ViewStudentComponent } from './Teacher/view-student/view-student.component';
import { ConfirmationComponent } from './Teacher/confirmation/confirmation.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AdminNavComponent } from './Admin/admin-nav/admin-nav.component';


@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    MyNavComponent,
    FileNotFoundComponent,
    StudentTableComponent,
    AuthenticationComponent,
    AdminRegisterComponent,
    AdminDashboardComponent,
    TeacherDashboardComponent,
    StudentDashboardComponent,
    ViewCourseComponent,
    ViewStudentComponent,
    ConfirmationComponent,
    AdminNavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    // AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    StudentRoutingModule,
    AppRoutingModule,
    SweetAlert2Module

  ],
  providers: [
    { provide: StudentService, useClass: StudentsFileImplService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
